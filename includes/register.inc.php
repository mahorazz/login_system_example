<?php
    include_once 'db_connect.php';
    include_once 'db_config.php';
    
    $error = "";
    
    if(isset($_POST['email'], $_POST['username'], $_POST['p'])){
        //puhastame sisendid ohtlikest jääkidest (script märgendid jms.)
        $username = filter_input(INPUT_POST,'username', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        //kui valideerimis filter tagastab "false" ei ole tegemist emailiga
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $error .= "<p class='error'>Email ei ole korrektne</p>";
        }
        
        $password = filter_input(INPUT_POST,'p',FILTER_SANITIZE_STRING);
        //kontrollime kas prüpteeritud parooli kuju on 128tähemärki pikk (sha512 standardi järgi)
        if(strlen($password) != 128){
            $error .= "<p class='error'>Vale parooli formaat</p>";
        }
        
        //kasutja nimi ja parooli õigus on kontrollitud kliendi poolel
        //nende väärtuste petmine ei anna otseselt eelist, seega ei hakka hetkel üle
        //kontrollima.
        
        //kontrollime kas selline email on juba registreeritud kellegi nimele
        if($stmt = $mysqli->prepare('SELECT id FROM members WHERE email = ? LIMIT 1;')){
            $stmt->bind_param('s',$email);
            $stmt->execute();
            $stmt->store_result();
            
            if($stmt->num_rows == 1){
                $error .= "<p class='error'>Selle emailiga kasutaja on juba olemas</p>";
                $stmt->close();
            }
        }else{
            $error .= "<p class='error'>Tekkis andmebaasi viga (email)</p>";
        }
        
        //kontrollime kas selline kasutajanimi on juba registreeritud
        if($stmt = $mysqli->prepare('SELECT id FROM members WHERE username = ? LIMIT 1;')){
            $stmt->bind_param('s',$username);
            $stmt->execute();
            $stmt->store_result();
            
            if($stmt->num_rows == 1){
                $error .= "<p class='error'>Selle nimega kasutaja on juba olemas</p>";
                $stmt->close();
            }
        }else{
            $error .= "<p class='error'>Tekkis andmebaasi viga (kasutajanimi)</p>";
        }
        
        /** TODO: kontrollima kas kasutajal on registreerimisõigused (rolli põhine lähenemine) **/
        
        //kontrollime kas on tekkinud vigu, kui ei ole, saadame ta "success" lehele
        if(empty($error)){
            //hashime parooli enne andmebaasi panekut ("soolame" üle)
            //sel juhul kontrollime õigusest kasutades password_verify() funktsiooni
            $password = password_hash($password, PASSWORD_BCRYPT);
            
            //lisame kasutaja andmebaasi members tabelisse
            if($insert_stmt = $mysqli->prepare("INSERT INTO 
            members(username,email,password) VALUES(?,?,?)")){
                $insert_stmt->bind_param('sss',$username,$email,$password);
                
                if(! $insert_stmt->execute()){
                    //insert lause ei saanud hakkama - arvatavasti andmebaasi ühenduse probleem
                    header('Location: ../error.php?err=Registration failure: INSERT');
                }
            }
            header('Location: register_success.php');
        }
    }