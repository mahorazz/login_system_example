<?php
    include_once "db_config.php";
    
    function sec_session_start(){
        $session_name = 'sec_session_id'; //anname sessioonile oma nime
        $secure = SECURE;
        
        //peatame Javascripti juurdepääsu küpsise andmetele (sessiooni id-le)
        $httponly = true;
        //surume kasutama ainult küpsiseid
        if(ini_set('session.use_only_cookies', 1) === FALSE){
            header('Location: ../error.php?err=Could not initiate safe session(ini_set)');
            exit();
        }
        
        //võtame küpsise parameetrid
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams['lifetime'],$cookieParams['path'], 
            $cookieParams['domain'], $secure, $httponly);
        session_name($session_name); //anname sessioonile nime edasi
        session_start(); //alustab PHP sessiooni
        session_regenerate_id(); //uuendame sessiooni, kustutades vana
    }
    
    function login($email, $password, $mysqli){
        //kasutame "ette valmistatud" SQL lauseid
        if($stmt = $mysqli->prepare('SELECT id,username,password FROM members 
            WHERE email = ? LIMIT 1;')){
            $stmt->bind_param('s',$email); //ühendame parameetri SQL lausega, asendades väärtuse ? alale
            $stmt->execute(); //käivitab ettevalmistatud lause
            $stmt->store_result();
            
            $stmt->bind_result($user_id,$username,$db_password);
            $stmt->fetch();
            
            //kontrollime kas sellise emailiga kasutaja on olemas
            if($stmt->num_rows == 1){
                //kontrollime ega selle emailiga pole juba korduvalt sisselogida üritatud
                if(checkbrute($user_id,$mysqli)){
                    //kui liiga palju ebaõnnestunud sisselogimis eksisteerib, lukustatakse konto
                    /* TODO: teavita kasutajat konto lukustatusest */
                    return false;
                }else{
                    //kontrollime parooli vastavust andmebaasis olevale
                    if(password_verify($password,$db_password)){
                        //küsime serverilt kliendi brauseri informatsiooni
                        $user_browser = $_SERVER['HTTP_USER_AGENT'];
                        //XSS rünnaku kaitse kasutaja ID peal
                        $user_id = preg_replace('/[^0-9]+/','',$user_id);
                        $_SESSION['user_id'] = $user_id;
                        
                        //XSS rünnaku kaitse kasutajanime peal
                        $username = preg_replace('/[^a-zA-Z0-9_]+/',"",$username);
                        $_SESSION['username'] = $username;
                        
                        $_SESSION['login_string'] = hash('sha512',$db_password.$user_browser);
                        //sisselogimine õnnestus
                        return true;
                    }else{
                        //parool ei olnud õige, salvestame luhtunud katse ja kellaaja
                        $now = time();
                        $mysqli->query("INSERT INTO login_attempts(user_id,time)
                        VALUES('$user_id','$now')");
                        return false;
                    }
                }
            }else{
                //sellise emailiga kasutajat polnud
                return false;
            }
        }else header('Location: ../error.php?err=Could not carry out sql query(user)');
    }
    
    function checkbrute($user_id,$mysqli){
        $now = time();
        //vaatame kas viimase 2 tunni jooksul on luhtunud sisselogimisi sellel kasutajal
        $valid_attempts = $now - (2*60*60);
        
        if($stmt = $mysqli->prepare("SELECT time 
        FROM login_attempts 
        WHERE user_id = ?
        AND time > '$valid_attempts'")){
            $stmt->bind_param('i',$user_id);
            
            $stmt->execute();
            $stmt->store_result();
            
            //kui on rohkem kui 5 katsed 2 tunni jooksul, blokeeri konto 
            if($stmt->num_rows > 5){
                //kui on rohkem kui 5 luhtunud katset
                return true;
            } else{
                //kui ei ole ületatud lubatut katsete arvu
                return false;
            }
        }
    }
    
    function login_check($mysqli){
        //kontrollime kas sessiooni muutujad on täidetud
        if(isset($_SESSION['user_id'],$_SESSION['username'],$_SESSION['login_string'])){
            $user_id = $_SESSION['user_id'];
            $username = $_SESSION['username'];
            $login_string = $_SESSION['login_string'];
            
            //küsime serveri käest kastuaja brauseri kohta infot
            $user_browser = $_SERVER['HTTP_USER_AGENT'];
            
            if($stmt = $mysqli->prepare('SELECT password FROM members WHERE id = ? LIMIT 1;')){
                $stmt->bind_param('i',$user_id);
                $stmt->execute();
                $stmt->store_result();
                
                if($stmt->num_rows == 1){
                    //kui kasutaja on olemas, salvestame saadud parooli andmebaasist(hashitud kujul)
                    $stmt->bind_result($password);
                    $stmt->fetch();
                    
                    $login_check = hash('sha512',$password.$user_browser);
                    //kontrollime kas hashitud parool ja brauseri andmed on samad, mis sessioni alguses
                    if(hash_equals($login_check,$login_string)){
                        //sisse logitud
                        return true;
                    }else{
                        //teine brauser/parool kui sessiooni alguses (session hijacking kaitse)
                        return false;
                    }
                }else{
                    //sisselogimine ebaõnnestus (selle id-ga kasutajat ei leitud)
                    return false;
                }
            }else{
                //prepare statment-i ei saanud luua (andmebaasi viga?)
                return false;
            }
        }else{
            //sessiooni muutujad pole täidetud ehk sessioon pole alustatud
            return false;
        }
    }
    
    function esc_url($url){
        
        if('' == $url){
            return $url;
        }
        
        $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i','',$url);
        $strip = array('%0d','%0a','%0D','%0A');
        $url = (string) $url;
        
        $count = 1;
        while($count){
            //eemaldame seni, kuni eemaldusi ühe tsükli sammu jooksul on 0
            $url = str_replace($strip,'',$url,$count);
        }
        
        $url = str_replace(';//','://', $url);
        $url = htmlentities($url);
        
        $url = str_replace('&amp;','&#038;',$url);
        $url = str_replace("'",'&#039;',$url);
        
        if($url[0] !== '/'){
            //meid huvitavad ainult lingid, mis asuvad meie enda serveris
            return '';
        }else{
            return $url;
        }
    }