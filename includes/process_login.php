<?php
    include_once 'db_connect.php';
    include_once 'functions.php';
    
    sec_session_start(); //alustame enda tehtud sessiooni
    
    if(isset($_POST['email'], $_POST['p'])){
        $email = $_POST['email'];
        $password = $_POST['p']; //javascripti poolt krüpteeritud parool
        
        if(login($email,$password,$mysqli)){
            header('Location: ../protected_page.php');
        }else{
            header('Location: ../index.php?error=1');
        }
    } else{
        echo 'invalid request';
    }