<?php
    include_once 'functions.php';
    sec_session_start(); //alustame enda tehtud sessiooni
    
    //tühjendame kõik sessiooni muutujad
    $_SESSION = array();
    
    //võtame küpsise kõik parameetrid
    $params = session_get_cookie_params();
    
    //"kustutame" küpsise ehk "invalidate cookie"
    //küpsise ülesehitus - name,value,expires,path,domain,secure,httponly
    setcookie(session_name(),
        '', 
        time() -42000,
        $params['path'],
        $params['domain'],
        $params['secure'],
        $params['httponly']
        );
    
    //hävitame sessiooni
    session_destroy();
    header('Location: ../index.php');