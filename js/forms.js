//sisselogimisvormi hash funtsioon
function formhash(form, password){
    if(form.email.value == ""){
        form.email.focus();
        return false;
    }
    if(form.password.value == ""){
        form.password.focus();
        return false;
    }
    //loome uue välja, kuhu pannakse krüpteeritud kujul parool
    let p = document.createElement("input");
    
    //lisame tehtud uue välja vormile, peidame selle ja lisame sinna krüpteeritud parooli
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
    
    //kustutame vormi andetest krüpteerimatta parooli
    password.value = "";
    
    //saadame vormi ära läbi javascripti
    form.submit();
}

//registratsiooni vormi hash funktsioon
function regformhash(form,uid,email,password,conf){
    console.log("sain siia");
    if(  uid.value == "" ||
       email.value == "" ||
    password.value == "" ||
         conf.value== ""){
        alert('Kõik väljad tuleb täita. Proovige uuesti.');
        return false;
    }
    
    //kontrollime kasutajanime
    let regex = /^\w+$/;
    if(!regex.test(form.username.value)){
        alert("Kasutajnimi võib sisaldada ainult tähti, numbreid ja alakriipse");
        form.username.focus();
        return false;
    }
    
    //kontrollime kas parool on piisavalt pikk ja sisaldab 1 suurt tähte,
    //1 väikest tähte ja 1 numbrit
    if(password.value.length < 8){
        alert('Parool peab olea vähemalt 8 tähemärki pikk');
        form.password.focus();
        return false;
    }
    regex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    if(!regex.test(password.value)){
        alert("Parool peab sisaldama vähemalt 1 suurt tähte, 1 väikest tähte ja 1 numbrit");
        return false;
    }
    
    //kontrollime kas parool vastab parooli kinnitusele
    if(password.value != conf.value){
        alert("Parool ja parooli kinnitus ei ole sama.");
        form.password.focus();
        return false;
    }
    
    //loome uue välja, kuhu pannakse krüpteeritud kujul parool
    let p = document.createElement("input");
    
    //lisame tehtud uue välja vormile, peidame selle ja lisame sinna krüpteeritud parooli
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
    
    //kustutame vormi andetest krüpteerimatta parooli
    password.value = "";
    conf.value = "";
    
    //saadame vormi ära läbi javascripti
    form.submit();
    return true;
}