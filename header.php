<?php
    include_once 'includes/register.inc.php';
    include_once 'includes/functions.php';
    sec_session_start();
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Secure login: Log In</title>
        <script type="text/javascript" src="js/sha512.js"></script>
        <script type="text/javascript" src="js/forms.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="css/styles.css" type="text/css" />
    </head>
    <body>
        <div class="container">
            <div class="header">
                <nav class="">
                    <ul class="nav nav-tabs">
                        <li><a class="nav-item nav-link">Home</a></li>
                        <li><a class="nav-item nav-link">About</a></li>
                    <?php if(!login_check($mysqli)):?> 
                        <li><a class="nav-item nav-link" href="login.php">Login</a></li>
                    </ul>
                    <?php else: ?>
                        <li><a class="nav-item nav-link" href="includes/logout.php">Logout</a></li>
                    </ul>
                    <ul class="nav">
                        <li><a class="nav-item nav-link" href="#">Todo</a></li>
                        <li><a class="nav-item nav-link" href="#">Inventory</a></li>
                        <li><a class="nav-item nav-link" href="#">Profile</a></li>
                    </ul>
                    <?php endif; ?>
                </nav>
            </div>
            <div class="content">
                
