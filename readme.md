CREATE DATABASE secure_login;

CREATE USER sec_user@'localhost' IDENTIFIED BY 'eKcGZr59zAa2BEWU';
GRANT SELECT, INSERT, UPDATE ON secure_login.* TO sec_user;

CREATE TABLE secure_login.members(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password CHAR(128) NOT NULL
);

CREATE TABLE secure_login.login_attempts(
    user_id INT NOT NULL,
    time VARCHAR(30) NOT NULL
);

INSERT INTO `secure_login`.`members` VALUES(1, 'test_user', 'test@example.com',
'$2y$10$IrzYJi10j3Jy/K6jzSLQtOLif1wEZqTRQoK3DcS3jdnFEhL4fWM4G');

CREATE TABLE todo(
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description VARCHAR(255) NOT NULL,
    user INT,
    CONSTRAINT usertask_fk FOREIGN KEY(user) REFERENCES members(id);
);