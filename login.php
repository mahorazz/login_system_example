<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    include_once 'header.php';
?>


    
<div class="d-flex justify-content-center">
    <div class="card form-container">
        <?php if(isset($_GET['error'])) echo '<p class="error">Sisselogimine ebaõnnestus!</p>'; ?>
        <form action="includes/process_login.php" method="post" class="align-center">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <label for="email" class="input-group-text">
                        <i class="fas fa-user"></i>
                    </label>
                </div>
                <input type="text" name="email" id="email" placeholder="email" class="form-control"/>
            </div>
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <label for="password" class="input-group-text">
                        <i class="fas fa-key"></i>
                    </label>
                </div>
                <input type="password" name="password" id="password" placeholder="password" class="form-control"/>
            </div>
            <div class="d-flex justify-content-center mt-3">
                <input type="button" value="Logi sisse" class="btn btn-primary" onclick="return formhash(this.form,this.form.password);"/>
            </div>
        </form>
    <div class="card-footer">
        <?php 
            if(login_check($mysqli)){
                echo '<p>Olete ' . $logged . ' logitud '. htmlentities($_SESSION['username']) . ' kontoga</p>';
                echo '<p><a href="includes/logout.php">Logi välja</a></p>';
            }else{
                echo '<p>Olete ' . $logged .' logitud </p>';
                echo '<p>Kui teil pole kasutajat, <a href="register.php">registreeri</a> </p>';
            }
        ?>
    </div>
    </div>  
    
</div>
    

<?php include_once "footer.php"; ?>