<?php include_once "header.php"; ?>
        <h1> Registeeri konto</h1>
        <div id="message">
            <?php 
                if(!empty($error)){
                    echo $error;
                }
            ?>
        </div>
        <div id="register_form">
            <ul>
                <li>Kasutajanimi võib ainult sisaldada tähti, numbreid ja alakriipsu</li>
                <li>email peab olema korrektne</li>
                <li>Parool peab sisaldama:
                    <ul>
                        <li>Vähemalt 8 tähemärki</li>
                        <li>Vähemalt ühte suurt tähte (A..Z)</li>
                        <li>Vähemalt ühte väikest tähte (a..z)</li>
                        <li>Vähemalt ühte numbrit (0..9)</li>
                    </ul>
                </li>
                <li>Parooli kinnitus peab vastama paroolile</li>
            </ul>
            <form action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" 
                method="post" name="registration_form">
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="username"/>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email"/>
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password"/>
                </div>
                <div class="form-group">
                    <label for="confirmpwd">Password confirm:</label>
                    <input type="password" name="confirmpwd" id="confirmpwd"/>
                </div>
                <input type="button" value="Registreeri" onclick="return regformhash(this.form, this.form.username, this.form.email, 
                this.form.password, this.form.confirmpwd);"/>
            </form>
        </div>

<?php include_once "footer.php"; ?>